<?php

namespace RestClient\ClientProxy;

class Proxy
{
    /**
     * @var string
     */
    private $host;

    /**
     * @var int
     */
    private $port;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @param string  $host
     * @param int     $port
     * @param string  $username
     * @param string  $password
     */
    public function __construct( $host , $port = 8080 , $username = null , $password = null )
    {
        $this->host =   (string)$host;
        $this->port =   (int)$port;

        if( $username && $password )
        {
            $this->username =   (string)$username;
            $this->password =   (string)$password;
        }
    }

    /**
     * @return bool
     */
    public function isAuthenticated()
    {
        return $this->password && $this->username;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @return string|null
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string|null
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * some.hostname.or.ip.com:1234
     *
     * @return string
     */
    public function getHostAndPort()
    {
        return $this->getHost() .':'. $this->getPort();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getHostAndPort();
    }
}