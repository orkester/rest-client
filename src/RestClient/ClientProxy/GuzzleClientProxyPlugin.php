<?php
namespace RestClient\ClientProxy;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class GuzzleClientProxyPlugin
 */
class GuzzleClientProxyPlugin implements EventSubscriberInterface
{

    /**
     * @var ProxyCollection
     */
    private $proxies;

    /**
     * @var bool
     */
    private $useHttpTunnel  =   false;

    /**
     * @param ProxyCollection $proxies
     * @param bool            $useHttpTunnel
     */
    public function __construct( ProxyCollection $proxies , $useHttpTunnel = false )
    {
        $this->proxies          =   $proxies;
        $this->useHttpTunnel    =   (bool)$useHttpTunnel;
    }

    /**
     * @{inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            'request.before_send' => 'onBeforeSend'
        );
    }

    /**
     * Calculate and set the proxy to use
     *
     * @param \Guzzle\Common\Event $event
     */
    public function onBeforeSend(\Guzzle\Common\Event $event)
    {
        $request =  $event['request'];
        $proxy   =  $this->proxies->getProxy();

        $request->getCurlOptions()->set( CURLOPT_PROXY , $proxy->getHost() );
        $request->getCurlOptions()->set( CURLOPT_PROXYPORT , $proxy->getPort() );
        $request->getCurlOptions()->set( CURLOPT_HTTPPROXYTUNNEL , $this->useHttpTunnel );

//        CURLOPT_FOLLOWLOCATION

        if( $proxy->isAuthenticated() )
        {
            $request->getCurlOptions()->set( CURLOPT_PROXYUSERPWD , $proxy->getUsername() .':'. $proxy->getPassword() );
        }
    }
}
