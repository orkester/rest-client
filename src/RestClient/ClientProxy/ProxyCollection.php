<?php

namespace RestClient\ClientProxy;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ProxyCollection implements \Iterator , \Countable
{
    /**
     * @var array
     */
    private $proxies    =   array();

    /**
     * @var int
     */
    private $count      =   0;

    /**
     * @var int
     */
    private $current    =   0;

    /**
     * @var bool
     */
    private $shuffleOnRewind  =   false;

    /**
     * $proxies can be \Traversable or an array of
     *
     *      strings 'some.host.com:1234' or '123.456.789.123:1234'
     *      arrays  [ 'host' => , 'port' => , 'username' => , 'password' => ]  ( username and password are optional )
     *      instances of Proxy
     *
     * @param array|\Traversable $proxies
     * @param bool               $shuffleOnRewind shuffle proxies on rewind (be aware that it rewinds in constructor too)
     * @throws \RuntimeException if $proxies is empty
     * @throws \InvalidArgumentException if proxy item is in invalid format
     */
    public function __construct( $proxies , $shuffleOnRewind = false )
    {
        if( !is_array( $proxies ) && !( $proxies instanceof \Traversable ) )
        {
            throw new \InvalidArgumentException( '"proxies" argument must be an array or Traversable' );
        }

        foreach( $proxies as $key => $proxy )
        {
            if( $proxy instanceof Proxy )
            {
                $this->add( $proxy );
            }
            else if( is_array( $proxy ) )
            {
                if( empty( $proxy['host'] ) || empty( $proxy['port'] ) )
                {
                    throw new \InvalidArgumentException( 'invalid proxy format' );
                }

                $this->add( new Proxy(
                                        $proxy['host'] ,
                                        $proxy['port'] ,
                                        @$proxy['username'] ,
                                        @$proxy['password']
                                     ) );
            }
            else if( false !== strpos( $proxy , ':' ) )
            {
                $proxy    =   explode( ':' , $proxy );
                $this->add( new Proxy( $proxy[0] , $proxy[1] ) );
            }
            else
            {
                throw new \InvalidArgumentException( 'invalid proxy format' );
            }

            if( empty( $this->proxies ) )
            {
                throw new \RuntimeException('At least one proxy has to be defined');
            }
        }

        $this->shuffleOnRewind( $shuffleOnRewind );
        $this->rewind();
    }

    /**
     * @param Proxy $proxy
     * @return $this
     */
    public function add( Proxy $proxy )
    {
        $this->proxies[]    =   $proxy;
        $this->count        =   count( $this->proxies );

        return $this;
    }

    /**
     * @return Proxy
     */
    public function current()
    {
        return $this->proxies[$this->current];
    }

    /**
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        $this->current++;
    }

    private function rewindIfNotValid()
    {
        if( !$this->valid() )
        {
            $this->rewind();
        }
    }

    /**
     * Returns the current proxy and advances the pointer. If the iterator gets exhausted, rewinds it.
     *
     * @return Proxy
     */
    public function getProxy()
    {
        $current    =   $this->current();

        $this->next();
        $this->rewindIfNotValid();

        return $current;
//        return $this->proxies[( $this->current++ % $this->count )];
    }

    /**
     * Return the key of the current element
     *
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return $this->current;
    }

    /**
     * Checks if current position is valid
     *
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     *       Returns true on success or false on failure.
     */
    public function valid()
    {
        return ( $this->current < $this->count );
    }

    /**
     * Rewind the Iterator to the first element
     *
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        $this->current  =   0;

        if( $this->shuffleOnRewind )
        {
            $this->shuffle();
        }
    }

    /**
     * @return int
     */
    public function count()
    {
        return $this->count;
    }

    /**
     * @return $this
     */
    public function shuffle()
    {
        shuffle( $this->proxies );

        return $this;
    }

    /**
     * shuffle each rewind?
     *
     * @param bool $shuffle
     * @return $this
     */
    public function shuffleOnRewind( $shuffle )
    {
        $this->shuffleOnRewind  =   (bool)$shuffle;

        return $this;
    }

    /**
     * @return Proxy
     */
    public function random()
    {
        return $this->proxies[array_rand( $this->proxies )];
    }

    /**
     * checks if session key ('proxy_collection') exists and whether it is an instance of static.
     * if yes, returns it.
     * if not, creates a new static() , stores it into session and returns it.
     *
     * this is useful when you want to keep the state of the ProxyCollection's iterator between requests to your scripts
     *
     * @param SessionInterface $session
     * @param array|\Traversable $proxies
     * @param bool             $shuffleOnRewind
     * @param string           $key
     * @return static
     */
    public static function getInstance( SessionInterface $session , $proxies , $shuffleOnRewind = false , $key = 'proxy_collection' )
    {
        if( $session->has( $key ) && $session->get( $key ) instanceof static )
        {
            $collection =   $session->get( $key );
        }
        else
        {
            $collection =   new static( $proxies , $shuffleOnRewind );
            $session->set( $key , $collection );
        }

        return $collection;
    }
}