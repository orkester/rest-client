<?php

namespace RestClient\ClientProxy;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

class PublicProxyCollection extends ProxyCollection
{
    const TYPE_HTTPS    =   'https';
    const TYPE_HTTP     =   'http';
    const TYPE_BOTH     =   'both';

    /**
     * @param string $type http|https|both
     * @param bool   $shuffleOnRewind
     * @throws \InvalidArgumentException
     */
    public function __construct( $type = self::TYPE_BOTH , $shuffleOnRewind = false )
    {
        switch( $type )
        {
            case    self::TYPE_HTTP     :   $proxies    =   $this->loadHttp();
                                            break;
            case    self::TYPE_HTTPS    :   $proxies    =   $this->loadHttps();
                                            break;
            case    self::TYPE_BOTH     :   $proxies    =   $this->loadHttps() + $this->loadHttp();
                                            break;
            default:    throw new \InvalidArgumentException( sprintf( '$type can be only %s|%s|%s , you gave %s' , self::TYPE_HTTP , self::TYPE_HTTPS , self::TYPE_BOTH , gettype( $type ) ) );
                        break;
        }

        parent::__construct( $proxies , $shuffleOnRewind );
    }

    /**
     * @return array
     */
    protected function loadHttps()
    {
        return array(
            '194-255-66-56.ifs.grenaas.net:8080' ,
            'ics161-254.icsincorporated.com:8080' ,
            'h88-150-186-204.host.redstation.co.uk:8090' ,
            '230-49-190-199.static.rcwmoab.com:8080' ,
            'romulus.core.gelsen.net:8081' ,
            '213.163.122.167:8080' ,
            '165.ip-37-187-54.eu:3128' ,
            '188.234.248.85:3128' ,
            '188.120.226.111:8888' ,
            '176.106.240.1:3128' ,
            '217.23.67.68:8080' ,
            '217.23.67.67:8080' ,
            '217.23.67.66:8080' ,
            '88.135.0.32:3128' ,
            '88.198.107.187:8888' ,
            '91.121.136.186:9999' ,
            '188.165.85.115:3128' ,
            '46.105.37.21:3128' ,
                    );
    }

    private function errorneousHttps()
    {
        return array(
            '81.25.172.105:3128' , //403
            '85.95.169.202:3128' , //403
            '95.80.67.74:3128' , //error
            '91.218.244.226:6588' , //error connreset
            '88.208.202.21:8080' , //error
            '212.3.153.25:3128' ,//403
            '109.198.192.144:3128' , //403
            '195.66.157.230:8080' , //error
            'andromeda.cloud.tilaa.com:3128' , //error
        );
    }

    private function errorneousHttp()
    {
        return array(
            '94-154-27-15.zapnet-isp.net:8090' , //error
            '89-109-52-236.static.mts-nn.ru:3128' , //error transfer closed
            '109-207-61-138.rdnet.pl:8090' , //error
            'logoprom26.starlink.ru:8080' , //error
            'zimbra.windoors.ru:5555' , //error
            '213.175.195.147:5555' , //error
            '190.122.185.218:8080' , //error
        );
    }

    /**
     * @return array
     */
    protected function loadHttp()
    {
        return array(
            '162.243.231.160:8080' ,
            '178.253.253.110:6666' ,
            '199.200.120.114:7808' ,
            '162.243.74.175:3128' ,
        );
    }

    /**
     * checks if session key ('public_proxy_collection') exists and whether it is an instance of static.
     * if yes, returns it.
     * if not, creates a new static() , stores it into session and returns it.
     *
     * this is useful when you want to keep the state of the ProxyCollection's iterator between requests to your scripts
     *
     * @param SessionInterface $session
     * @param string           $type
     * @param bool             $shuffleOnRewind
     * @param string           $key
     * @return static
     */
    public static function getInstance( SessionInterface $session , $type = self::TYPE_BOTH , $shuffleOnRewind = false , $key = 'public_proxy_collection' )
    {
        return parent::getInstance( $session , $type , $shuffleOnRewind , $key );
    }
}