<?php

namespace RestClient;

use Symfony\Component\DomCrawler\Form;

class FormSubmitter extends CrawlerClient
{
    /**
     * @param array $data
     * @param Form  $form
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function submit( array $data , Form $form )
    {
        $this->prepareClient();

        $crawler    =   $this->getGoutteClient()->submit( $form , $data );

        return $crawler;
    }
}