<?php

namespace RestClient;

use Psr\Log\LoggerInterface;
use Goutte\Client as GoutteClient;
use Guzzle\Http\Exception\BadResponseException;
use Guzzle\Http\Exception\CurlException;
use Guzzle\Http\Exception\RequestException;
use Guzzle\Common\Event as GuzzleEvent;
use Guzzle\Log\PsrLogAdapter as GuzzlePsrLogAdapter ,
    Guzzle\Plugin\Log\LogPlugin as GuzzleLogPlugin;
use RestClient\ClientProxy\ProxyCollection ,
    RestClient\ClientProxy\GuzzleClientProxyPlugin;

class RestClient
{
    const   CONTENT_JSON        =   'application/json';
    const   CONTENT_XML         =   'application/xml';
    const   CONTENT_HTML        =   'text/html';
    const   CONTENT_ANY         =   '*/*';
    const   CONTENT_URLENCODED  =   'application/x-www-form-urlencoded';
    const   CONTENT_MULTIPART   =   'multipart/form-data';

    const   METHOD_GET          =   'GET';
    const   METHOD_POST         =   'POST';
    const   METHOD_PUT          =   'PUT';
    const   METHOD_PATCH        =   'PATCH';
    const   METHOD_DELETE       =   'DELETE';
    const   METHOD_HEAD         =   'HEAD';

    /**
     * @var array
     */
    protected   $headers          =   array();

    /**
     * @var \Goutte\Client
     */
    protected   $client;

    /**
     * @var string
     */
    protected   $baseUrl    =   '';

    /**
     * @var string
     */
    protected   $responseContentType    =   self::CONTENT_JSON;

    /**
     * @var \Guzzle\Http\Message\Response
     */
    protected   $lastResponse;

    /**
     * @var \Exception
     */
    protected   $lastError;

    /**
     * @var LoggerInterface
     */
    protected   $logger;

    /**
     * @var ProxyCollection
     */
    protected   $proxies;

    /**
     * @param string $baseUrl
     * @param LoggerInterface $logger
     */
    public function __construct( $baseUrl = null , LoggerInterface $logger = null )
    {
        $this->client   =   new GoutteClient();

        if( $baseUrl )
        {
            $this->setBaseUrl( $baseUrl );
        }

        if( $logger )
        {
            $this->setLogger( $logger );
        }
    }

    /**
     * @return \Guzzle\Http\Client
     */
    public function getGuzzleClient()
    {
        return $this->client->getClient();
    }

    /**
     * @return \Goutte\Client
     */
    public function getGoutteClient()
    {
        return $this->client;
    }

    /**
     * @param LoggerInterface $logger
     * @return $this
     */
    public function setLogger( LoggerInterface $logger )
    {
        $this->logger   =   $logger;
        $this->getGuzzleClient()->addSubscriber( new GuzzleLogPlugin( new GuzzlePsrLogAdapter( $logger ) ) );

        return $this;
    }

    /**
     * @param string $baseUrl
     * @return $this
     */
    public function setBaseUrl( $baseUrl )
    {
        $this->baseUrl  =   (string)$baseUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param array $headers
     * @return $this
     */
    public function setHeaders( array $headers )
    {
        $this->headers  =   $headers;

        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers +
        [
            'Accept'        =>  $this->getResponseContentType() ,
            'User-Agent'    =>  'kor3k/rest-client' ,
            'Cache-Control' =>  'no-cache' ,
        ];
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setResponseContentType( $type )
    {
        $this->responseContentType  =   (string)$type;

        return $this;
    }

    /**
     * @return string
     */
    public function getResponseContentType()
    {
        return $this->responseContentType;
    }

    /**
     * @return array
     */
    public function getCamouflageHeaders()
    {
        return array(
            'Referer'	    =>  (string)@parse_url( $this->getBaseUrl() )['scheme']. '://' .(string)@parse_url( $this->getBaseUrl() )['host'] ,
            'Host'          =>  str_ireplace( [ 'https:' , 'http:' , '/' ] , '' , (string)@parse_url( $this->getBaseUrl() )['host'] ) ,
            'User-Agent'	=>  'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)' ,
            'Content-Type'	=>  'application/json; charset=utf-8' ,
            'Accept'        =>  'application/json' ,
        );
    }


    /**
     * @param string $uri
     * @param array $headers
     * @param mixed $body
     * @return array|bool|float|\Guzzle\Http\EntityBodyInterface|int|\SimpleXMLElement|string
     */
    public function get( $uri , $headers = array() , $body = null )
    {
        return $this->request( static::METHOD_GET , $uri , $headers , $body );
    }

    /**
     * @param string $uri
     * @param array $headers
     * @param mixed $body
     * @return array|bool|float|\Guzzle\Http\EntityBodyInterface|int|\SimpleXMLElement|string
     */
    public function post( $uri , $headers = array() , $body = null )
    {
        return $this->request( static::METHOD_POST , $uri , $headers , $body );
    }

    /**
     * @param       $uri
     * @param array $headers
     * @param mixed $body
     * @return array|bool|float|\Guzzle\Http\EntityBodyInterface|int|\SimpleXMLElement|string
     */
    public function put( $uri , $headers = array() , $body = null )
    {
        return $this->request( static::METHOD_PUT , $uri , $headers , $body );
    }

    /**
     * @param string $uri
     * @param array $headers
     * @param mixed $body
     * @return array|bool|float|\Guzzle\Http\EntityBodyInterface|int|\SimpleXMLElement|string
     */
    public function patch( $uri , $headers = array() , $body = null )
    {
        return $this->request( static::METHOD_PATCH , $uri , $headers , $body );
    }

    /**
     * @param string $uri
     * @param array $headers
     * @param mixed $body
     * @return array|bool|float|\Guzzle\Http\EntityBodyInterface|int|\SimpleXMLElement|string
     */
    public function delete( $uri , $headers = array() , $body = null )
    {
        return $this->request( static::METHOD_DELETE , $uri , $headers , $body );
    }

    /**
     * @param string $uri
     * @param array $headers
     * @return array|bool|float|\Guzzle\Http\EntityBodyInterface|int|\SimpleXMLElement|string
     */
    public function head( $uri , $headers = array() )
    {
        return $this->request( static::METHOD_HEAD , $uri , $headers );
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $headers
     * @param mixed  $body
     * @return array|bool|float|\Guzzle\Http\EntityBodyInterface|int|\SimpleXMLElement|string
     * @throws \Exception|RequestException|CurlException|BadResponseException|GuzzleException
     */
    public function request( $method , $uri , $headers = array() , $body = null )
    {
        $this->lastResponse =   null;
        $request = $this->createRequest( $method , $uri , $headers , $body );

        if( $this->logger )
        {
            $this->logRequest( $method , $uri , $request->getHeaders()->toArray() , $body );
        }

        try
        {
            $this->lastResponse   =   $request->send();

            if( $this->logger )
            {
                $this->logResponse(
                                    $this->lastResponse->getBody() ,
                                    $this->lastResponse->getStatusCode() ,
                                    $this->lastResponse->getHeaders()
                                  );
            }

            if( static::CONTENT_JSON === $this->getResponseContentType() )
            {
                return $this->lastResponse->json();
            }
            else if( static::CONTENT_XML === $this->getResponseContentType() )
            {
                return $this->lastResponse->xml();
            }
            else
            {
                return $this->lastResponse->getBody();
            }
        }
        catch( \Exception $e )
        {
            if( $this->logger )
            {
                $this->logException( $e );
            }

            $this->lastError    =   $e;
            throw $e;
        }
    }

    /**
     * @param \Exception $e
     */
    protected function logException( \Exception $e )
    {
        $context    =   [
                            'message'   =>  $e->getMessage() ,
                            'file'      =>  $e->getFile() ,
                            'line'      =>  $e->getLine() ,
                            'stack'     =>  $e->getTraceAsString() ,
                        ];

        if( $this->lastResponse )
        {
            $context['response']    =   (string)$this->lastResponse->getBody();
        }

        if( $e instanceof RequestException )
        {
            $context['url']     =   $e->getRequest()->getUrl();

            if( $e instanceof CurlException )
            {
                $context['error'] =   $e->getError();
            }

            if( $e instanceof BadResponseException )
            {
                $context['status code']     =   $e->getResponse()->getStatusCode();
                $context['reason phrase']   =   $e->getResponse()->getReasonPhrase();
            }
        }

        $this->logger->alert( get_class( $e ) , $context );
    }

    /**
     * @return \Guzzle\Http\Message\Response
     */
    public function getLastResponse()
    {
        return $this->lastResponse;
    }

    /**
     * @return \Exception|RequestException|CurlException|BadResponseException|GuzzleException
     */
    public function getLastError()
    {
        return $this->lastError;
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $headers
     * @param mixed  $body
     * @return \Guzzle\Http\Message\RequestInterface
     */
    protected function createRequest( $method , $uri , $headers = array() , $body = null )
    {
        return $this->getGuzzleClient()
                            ->createRequest( $method , $this->getBaseUrl() . $uri , $headers + $this->getHeaders() , $body );
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $headers
     * @param mixed  $body
     * @return array
     */
    protected function logRequest( $method , $uri , $headers = array() , $body = null )
    {
        $request    =   [
            'method'    =>  $method ,
            'url'       =>  $this->getBaseUrl() . $uri ,
            'headers'   =>  $headers ,
            'body'      =>  $body ,
        ];

        $this->logger->info( 'RestClient request' , $request );

        return $request;
    }

    /**
     * @param mixed $body
     * @param int $statusCode
     * @param array $headers
     * @return array
     */
    protected function logResponse( $body , $statusCode , $headers )
    {
        $response   =   [
            'body'      =>  (string)$body ,
            'status'    =>  $statusCode ,
            'headers'   =>  $headers ,
        ];

        $this->logger->info( 'RestClient response' , $response );

        return $response;
    }

    /**
     * @param bool $verify
     * @return $this
     */
    public function verifySsl( $verify = true )
    {
        if( $verify )
        {
            $this->getGuzzleClient()->setSslVerification( true , 1 , 2 );
        }
        else
        {
            $this->getGuzzleClient()->setSslVerification( false , 0 , 0 );
        }

        return $this;
    }

    /**
     * @param string $user
     * @param string $password
     * @param int $type CURLAUTH_* constant
     * @return $this
     */
    public function setHttpAuth( $user , $password = '' , $type = CURLAUTH_BASIC )
    {
        $this->getGuzzleClient()->getEventDispatcher()->addListener( 'request.before_send' ,
        function( GuzzleEvent $event ) use( $user , $password , $type )
        {
            $request    =   $event['request'];
            $request->setAuth(
                $user,
                $password,
                $type
            );
        });

//        $client->setDefaultOption('auth', array('username', 'password', 'Basic|Digest|NTLM|Any'));

//        $this->getGoutteClient()->setAuth(
//            $this->auth['user'],
//            $this->auth['password'],
//            $this->auth['type']
//        );

        return $this;
    }

    /**
     * every request iterates over ProxyCollection
     *
     * @param ProxyCollection $proxies
     * @return $this
     */
    public function setProxies( ProxyCollection $proxies )
    {
        $this->proxies  =   $proxies;
        $this->getGuzzleClient()
                ->getEventDispatcher()
                    ->addSubscriber( new GuzzleClientProxyPlugin( $proxies ) );

        return $this;
    }

    /**
     * @return ProxyCollection|null
     */
    public function getProxies()
    {
        return $this->proxies;
    }
}