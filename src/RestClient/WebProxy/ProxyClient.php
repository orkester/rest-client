<?php

namespace RestClient\WebProxy;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Guzzle\Http\Exception\BadResponseException;
use Guzzle\Http\Url;
use RestClient\RestClient;
use Psr\Log\LoggerInterface;

class ProxyClient extends RestClient
{
    /**
     * @var string
     */
    protected $host;

    /**
     * @param string     $host
     * @param string     $baseUrl
     * @param LoggerInterface $logger
     */
    public function __construct( $host , $baseUrl = null , LoggerInterface $logger = null )
    {
        parent::__construct( $baseUrl , $logger );
        $this->setHost( $host );
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     * @return $this
     */
    public function setHost( $host )
    {
        $this->host =   (string)$host;
        return $this;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handleRequest( Request $request )
    {
        $this->lastResponse =   null;

        // URL of the proxied service is extracted from the options. The requested path
        // and query string are attached.
//        $url = Url::factory( $request->getRequestUri() );
//        $url->setQuery( $request->getQueryString() );

        $url    =   $request->getScheme() . '://' . $this->getHost() . $request->getBaseUrl() . $request->getPathInfo() . $request->getQueryString();

        $httpRequest    =   $this->createRequest(
                                                    $request->getMethod() ,
                                                    $url ,
                                                    $request->headers->all() ,
                                                    $request->getContent()
                                                );


        if( $this->logger )
        {
            $this->logRequest( $request->getMethod() , $url , $request->headers->all() , $request->getContent() );
        }

        try
        {
            $httpResponse = $httpRequest->send();
            $this->lastResponse =   $httpResponse;

            if( $this->logger )
            {
                $this->logResponse(
                    $this->lastResponse->getBody() ,
                    $this->lastResponse->getStatusCode() ,
                    $this->lastResponse->getHeaders()
                );
            }
        }
        catch( BadResponseException $e )
        {
            $httpResponse = $e->getResponse();

            if( $this->logger )
            {
                $this->logException( $e );
            }
        }

        // Stash the prepared Guzzle request and response in the Symfony request attributes
        // for debugging.
        $request->attributes->set('guzzle_request', $httpRequest);
        $request->attributes->set('guzzle_response', $httpResponse);

        $body = $httpResponse->getBody(true);
        $statusCode = $httpResponse->getStatusCode();

        // This cannot handle every response. Chunked transfer encoding would necessitate
        // a streaming response.
        $headers = $httpResponse->getHeaders()->toArray();
        unset($headers['Transfer-Encoding']);

        return new Response($body, $statusCode, $headers);
    }
}