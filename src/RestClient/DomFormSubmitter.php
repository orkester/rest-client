<?php

namespace RestClient;

use Symfony\Component\DomCrawler\Form;
use Symfony\Component\DomCrawler\Crawler;
use Psr\Log\LoggerInterface;

class DomFormSubmitter extends FormSubmitter
{
    /**
     * @var callable
     */
    protected   $formFinder;

    /**
     * callable receives DomCrawler\Crawler and must return DomCrawler\Form
     *
     * @param callable        $formFinder
     * @param string          $baseUrl
     * @param LoggerInterface $logger
     */
    public function __construct( callable $formFinder , $baseUrl , LoggerInterface $logger = null )
    {
        $this->formFinder   =   $formFinder;
        parent::__construct( $baseUrl , $logger );
    }

    /**
     * return $crawler->filter( 'input[name=odeslat]' )->form();
     *
     * @param Crawler $crawler
     * @throws \RuntimeException
     * @return Form
     */
    protected function findForm( Crawler $crawler )
    {
        $finder =   $this->formFinder;
        $form   =   $finder( $crawler );

        if( !( $form instanceof Form ) )
        {
            throw new \RuntimeException( '$formFinder callable must return an instance of Symfony\Component\DomCrawler\Form' );
        }

        return $form;
    }

    /**
     * @param array $data
     * @param Form  $form
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function submit( array $data , Form $form = null )
    {
        return parent::submit( $data , $this->getForm() );
    }

    /**
     * @return \Symfony\Component\DomCrawler\Form
     */
    public function getForm()
    {
        $this->prepareClient();

//        $crawler  =   $this->client->request( 'GET' , $this->getBaseUrl() , $this->getServerParameters() );

        return $this->findForm( $this->request( static::METHOD_GET , $this->getBaseUrl() ) );
    }
}