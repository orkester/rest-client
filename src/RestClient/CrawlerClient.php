<?php

namespace RestClient;

use Guzzle\Http\Message\Response;

class CrawlerClient extends RestClient
{
    /**
     * @var string
     */
    protected   $responseContentType    =   self::CONTENT_HTML;

    /**
     * @return array
     */
    public function getServerParameters()
    {
        $headers    =   $this->getHeaders();
        $ret        =   array();

        if( !empty( $headers['Referer'] ) )
        {
            $ret['HTTP_REFERER']  = $headers['Referer'];
        }
        if( !empty( $headers['Host'] ) )
        {
            $ret['REMOTE_HOST']  = $headers['Referer'];
            $ret['REMOTE_ADDR']  = gethostbyname( $headers['Host'] ) ;
        }

        return $ret;
    }

    /**
     * applies headers and server parameters to the client
     */
    protected function prepareClient()
    {
        foreach( $this->getHeaders() as $key => $val )
        {
            $this->client->setHeader( $key , $val );
        }unset($key,$val);

        foreach( $this->getServerParameters() as $key => $val )
        {
            $this->client->setServerParameter( $key , $val );
        }unset($key,$val);
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array  $headers
     * @param mixed   $body
     * @throws \Exception
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function request( $method , $uri , $headers = array() , $body = null )
    {
        $this->lastResponse =   null;
        $this->prepareClient();

        try
        {
            $crawler            =   $this->createRequest( $method , $uri , $headers , $body );
            $response           =   $this->getGoutteClient()->getResponse();
            $this->lastResponse =   new Response( $response->getStatus() , $response->getHeaders() , $response->getContent() );

            if( $this->logger )
            {
                $this->logRequest( $method , $uri , $this->getGoutteClient()->getInternalRequest()->getServer() , $body );
                $this->logResponse(
                    $this->lastResponse->getBody() ,
                    $this->lastResponse->getStatusCode() ,
                    $this->lastResponse->getHeaders()
                );
            }

            return $crawler;
        }
        catch( \Exception $e )
        {
            if( $this->logger )
            {
                $this->logException( $e );
            }

            $this->lastError    =   $e;
            throw $e;
        }
    }

    /**
     * @param       $method
     * @param       $uri
     * @param array $headers
     * @param null  $body
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    protected function createRequest( $method , $uri , $headers = array() , $body = null )
    {
        return $this->getGoutteClient()->request( $method , $this->getBaseUrl() . $uri , array() , array() , $headers , $body );
    }
}